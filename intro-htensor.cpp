#include "htensor.h"

int main()
{
    cout << endl << "'HTensor H( \"../sample_tensors/tensor1.ht\" )' loads tensor1.ht from the file into the object H." << endl << endl;
    HTensor H( "../sample_tensors/tensor1.ht" );

    cout << "'vector< int > N = H.mode_sizes()' returns a vector of size H.dim(), " << endl;
    cout << "filled with the sizes of the tensor directions:" << endl;
    vector< int > N = H.mode_sizes();
    cout << "N = [ ";
    for ( int i = 0; i < N.size(); ++i )
    {
        cout << N[ i ] << " ";
    }
    cout << "]" << endl << endl;

    cout << "The HTensor H can be sent to cout:" << endl;
    cout << H << endl;

    return 0;
}
