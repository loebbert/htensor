#include "matrix.h"
#include <algorithm>
#include <cstring>
#include <cassert>
#include <iomanip>
#include <string>

using namespace std;

//
// Constructs a zero-matrix of prescribed dimensions.
//
Matrix::Matrix(int rows, int cols, int data_size)
{
    _rows = rows;
    _cols = cols;

    _data_size = (data_size < rows * cols) ? rows * cols : data_size;
    _data = new double[ _data_size ];
    _data_from_outside = false;

    _reference_counting  = new int;
    *_reference_counting = 1;

    for ( int i = 0; i < _rows * _cols; ++i )
    {
        _data[ i ] = 0.0;
    }
}

//
// Constructs a matrix of prescribed data and dimensions.
// Caution: If external_data = false, the matrix data will be deleted 
//          by the Matrix destructor.
//
Matrix::Matrix( double * data, int rows, int cols, bool external_data, int data_size )
{
    _rows = rows;
    _cols = cols;

    _data = data;
    _data_size = ( data_size < rows * cols) ? rows * cols : data_size;
    _data_from_outside = external_data;

    _reference_counting  = new int;
    *_reference_counting = 1;
}

//
// Copy constructor. 
// The copy (this) points to the same data as the copied matrix M.
// The number of copies is counted and the data will be deleted on the destruction
// of the last copy (unless it is declared as outside data).
//
Matrix::Matrix( const Matrix & M )
{
    _rows = M._rows;
    _cols = M._cols;

    _data = M._data;
    _data_size = M._data_size;
    _reference_counting = M._reference_counting;
    (*_reference_counting)++;

    _data_from_outside = M._data_from_outside;
}

//
// Assignment.
// New matrix (this) gets the same _data pointer as the matrix M, i.e. the matrix data is not copied.
//
Matrix & Matrix::operator =( const Matrix & M )
{
    if ( &M == this )
    {
        // Do not copy yourself.
        return *this;
    }

    if ( _reference_counting != M._reference_counting )
    {
        (*_reference_counting)--;
        if ( *_reference_counting <= 0 )
        {
            if ( !_data_from_outside )
            {
                delete[] _data;
            }
            _data = 0;

            delete _reference_counting;
            _reference_counting = 0;
        }

        // Don't assign not referenced data.
        assert( *M._reference_counting > 0);

        _rows = M._rows;
        _cols = M._cols;
        _data = M._data;
        _data_size = M._data_size;

        _data_from_outside = M._data_from_outside;

        _reference_counting = M._reference_counting;
        (*_reference_counting)++;
    }

    return (*this);
}

//
// Destructor.
//
Matrix::~Matrix()
{
    (*_reference_counting)--;

    if ( (*_reference_counting) <= 0 )
    {
        if ( !_data_from_outside )
        {
            delete[] _data;
            _data = 0;
        }
        delete _reference_counting;
        _reference_counting = 0;
    }
}
  
//
// Resizes the matrix. If keep_data is false, all entries are set to zero.
// Otherwise, the top left block of the old matrix will be copied to the top
// left of the new matrix.
// Caution: _data is resized only if it has been allocated by the
//          constructor (and not from outside).
//
void Matrix::resize(int rows, int cols , bool keep_data)
{
    if( !keep_data )
    {
        *this = Matrix( rows, cols );
        return;
    }

    // There are 2 cases that have to be considered:
    //   1. There is enough data preallocated and ref_counting == 1                         -> reuse
    //   2. There is either not enough preallocated data or it is used by another object    -> copy
    double * copy_data;
    bool copied_data;
    if ( _data_size < rows * cols || (*_reference_counting) > 1 )
    {
        copy_data = new double[ max( _data_size, rows * cols ) ];
        _data_size = max( _data_size, rows * cols );
        copied_data = true;
    } 
    else 
    {
        copy_data = _data;
        copied_data = false;
    }

    // Copy the data to the correct position. 
    // If the new matrix is bigger, fill it up with zeros. 
    // To avoid overwriting data that is still needed, this is done from back to front.
    for ( int j = cols - 1; j >= 0; j-- )
    {
        for ( int i = rows - 1; i >= 0; i-- )
        {
            if ( i < _rows && j < _cols )
            {
                copy_data[ i + j * rows ] = _data[ i + j * _rows ];
            } 
            else 
            {
                copy_data[ i + j * rows ] = 0;
            }
        }
    }
    _rows = rows;
    _cols = cols;

    if ( copied_data )
    {
        (*_reference_counting)--;
        if( !_data_from_outside && (*_reference_counting) <= 0 )
        {
            delete[] _data;
        }

        if ( (*_reference_counting) <= 0 )
        {
            delete _reference_counting;
        }

        _reference_counting = new int;
        (*_reference_counting) = 1;
    }

    _data = copy_data;
}

//
// Reshape the matrix: interpret the given data as row x col matrix,
// it is necessary that rows * cols = _rows * _cols.
//
void Matrix::reshape(int rows, int cols)
{
    _rows = rows;
    _cols = cols;
}

//
// Element access (const).
//
double Matrix::operator ()( int i, int j ) const
{
    return _data[ i + j * _rows ];
}

//
// Element access (reference).
//
double & Matrix::operator ()( int i, int j )
{
    return _data[ i + j * _rows ];
}

//
// Returns i-th row of this as Matrix.
//
Matrix Matrix::row( int i ) const
{
    Matrix tmp( 1, _cols );

    for ( int j = 0; j < _cols; ++j )
    {
        tmp( 0, j ) = _data[ i + j * _rows ];
    }

    return tmp;
}

//
// Returns j-th column of this as Matrix.
//
Matrix Matrix::col( int j ) const
{
    Matrix tmp( _rows, 1 );

    for ( int i = 0; i < _rows; ++i )
    {
        tmp( i, 0 ) = _data[ i + j * _rows ];
    }

    return tmp;
}

//
// Returns a pointer to the internal data of this.
//
double * Matrix::data() const
{
    return _data;
}

//
// Copies the interal data (if it is also used for other matrices).
// If keep_allocation is true, the method will preallocate the same
// amount of memory it had before, regardless of the size of the matrix.
//
void Matrix::copy_data( bool keep_preallocation )
{
    if( *_reference_counting > 1 || _data_from_outside )
    {
        double * copy_data;
        if( keep_preallocation )
        {
            copy_data = new double[ max(_data_size, _rows * _cols) ];
            _data_size = max(_data_size, _rows * _cols);
        } else {
            copy_data = new double[ _rows * _cols ];
            _data_size = _rows * _cols;
        }
        memcpy(copy_data, _data, _rows * _cols * sizeof(double));
        _data_from_outside = false;


        // Replace the datapointer and decrement the reference counting variable:
        (*_reference_counting)--;
        if( *_reference_counting == 0 )
        {
            // The matrix had external data that is not used anymore.
            delete _reference_counting;
        }
        _reference_counting  = new int;
        *_reference_counting = 1;
        _data = copy_data;
    }
}

//
// Copies the Matrix M into the calling object. Does not affect matrices
// that previously referenced data from M. The new matrix still preallocates
// enough memory to store a minimum of _data_size entries.
//
void Matrix::copy_matrix_from(const Matrix & M)
{
    _cols = M._cols;
    _rows = M._rows;
    _data_from_outside = false;

    if( (*_reference_counting) > 1 )
    {
        // _data is used by other matrices, i.e. new data has to be allocated.
        // M keeps the preallocation, i.e. _data has a minimum of _data_size entries.
        (*_reference_counting)--;
        _reference_counting = new int;
        (*_reference_counting) = 1;

        _data_size = (_data_size > _rows * _cols) ? _data_size : _rows * _cols;
        _data = new double[ _data_size ];
    }
    else if( _data_size < M.rows() * M.cols() )
    {
        _data_size = _rows * _cols;
        delete[] _data;
        _data = new double[ _data_size ];

    }

    memcpy(_data,
           M._data,
           _rows * _cols * sizeof(double));
}

//
// Returns reference to j-th column of this as (sub_rows x sub_cols)-Matrix.
//
Matrix Matrix::col_matrix( int j, int rows, int cols ) const
{
    double * col;
    col = _data + j * _rows;
    Matrix col_matrix( col, rows, cols );
    return col_matrix;
}

//
// Scales this by alpha.
//
void Matrix::scale( double alpha )
{
    for ( int j = 0; j < _cols; ++j )
    {
        for ( int i = 0; i < _rows; ++i )
        {
            (*this)( i, j ) *= alpha;
        }
    }
}

//
// Computes Y = Y + alpha * X.
// X and Y should be of same dimensions.
//
void matrix_add( Matrix & Y, double alpha, const Matrix & X )
{
    int rows = min( X._rows, Y._rows );
    int cols = min( X._cols, Y._cols );

    for ( int j = 0; j < cols; ++j )
    {
        for ( int i = 0; i < rows; ++i )
        {
            Y( i, j ) += alpha * X( i, j );
        }
    }
}

//
// Computes the sclar product of two matrices.
//
double matrix_dot( const Matrix & A, const Matrix & B )
{
    int rows = min( A._rows, B._rows );
    int cols = min( A._cols, B._cols );

    double dot = 0.0;

    for ( int j = 0; j < cols; ++j )
    {
        for ( int i = 0; i < rows; ++i )
        {
            dot += A( i, j ) * B( i, j );
        }
    }

    return dot;
}

//
// Computes C = A * B.
//
void matrix_mul( const Matrix & A, const Matrix & B, Matrix & C )
{   
    C.resize( A._rows, B._cols );

    for ( int j = 0; j < B._cols; ++j )
    {
        for ( int k = 0; k < A._cols; ++k )
        {
            for ( int i = 0; i < A._rows; ++i )
            {
                C( i, j ) += A( i, k ) * B( k, j );
            }
        }
    }
}

//
// Computes C = A * B^T. 
//
void matrix_mul_NT( const Matrix & A, const Matrix & B, Matrix & C )
{
    C.resize( A._rows, B._rows );

    for ( int j = 0; j < B._rows; ++j )
    {
        for ( int k = 0; k < A._cols; ++k )
        {
            for ( int i = 0; i < A._rows; ++i )
            {
                C( i, j ) += A( i, k ) * B( j, k );
            }
        }
    }
}

//
// Computes C = A^T * B.
//
void matrix_mul_TN( const Matrix & A, const Matrix & B, Matrix & C )
{
    C.resize( A._cols, B._cols );

    for ( int j = 0; j < B._cols; ++j )
    {
        for ( int i = 0; i < A._cols; ++i )
        {
            for ( int k = 0; k < A._rows; ++k )
            {
                C( i, j ) += A( k, i ) * B( k, j );
            }
        }
    }
}

//
// Matrix to ostream.
//
ostream & operator <<( ostream & os, const Matrix & M )
{
    M.send_to_ostream_with_prefix( os );
    return os;
}

//
// Matrix to ostream with prefix.
//
void Matrix::send_to_ostream_with_prefix( ostream & os, const string & prefix ) const
{
    double tmp;

    os << scientific << setprecision( 3 );
    for ( int i = 0; i < _rows; ++i )
    {
        os << prefix;
        for ( int j = 0; j < _cols; ++j )
        {
            tmp = (*this)( i, j );
            os << tmp << "\t";
        }
        if ( i < _rows - 1 )
        {
            os << endl;
        }
    }
}
