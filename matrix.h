#ifndef _MATRIX_H
#define _MATRIX_H

#include <iostream>

using namespace std;

class Matrix
{
  private:
    int         _rows;
    int         _cols;
    double *    _data;
    int         _data_size;

    //! True if _data has been allocated outside (and will be deleted there).
    bool _data_from_outside;

    //! Reference counting. Counts all copies of this (this included).
    int * _reference_counting;

  public:

    //! Constructs a zero-matrix of prescribed dimensions.
    Matrix( int rows = 0, int cols = 0, int data_size = -1 );

    //! Constructs a matrix of prescribed data and dimensions.
    //! Caution: If external_data = false, the matrix data will be deleted 
    //!          by the Matrix destructor.
    Matrix( double * data, int rows, int cols, bool external_data = true,
           int data_size = -1 );

    //! Copy constructor. 
    //! The copy (this) points to the same data as the copied matrix M.
    //! The number of copies is counted and the data will be deleted on the destruction
    //! of the last copy (unless it is declared as outside data).
    Matrix( const Matrix & M );

    //! Assignment.
    //! New matrix (this) gets the same _data pointer as the matrix M, i.e. the matrix data is not copied.
    Matrix & operator =( const Matrix &M );

    //! Destructor.
    ~Matrix();

    int rows() const { return _rows; }
    int cols() const { return _cols; }

    //! Resizes the matrix. If keep_data is false, all entries are set to zero.
    //! Otherwise, the top left block of the old matrix will be copied to the top
    //! left of the new matrix.
    //! Caution: _data is resized only if it has been allocated by the
    //!          constructor (and not from outside).
    void resize( int rows = 0, int cols = 0, bool keep_data = false );

    //! Reshape the matrix: interpret the given data as row x col matrix,
    //! it is necessary that rows * cols = _rows * _cols.
    void reshape( int rows, int cols);

    //! Element access.
    double   operator ()( int i, int j ) const;
    double & operator ()( int i, int j );

    //! Returns i-th row of this as (1 x #rows)-Matrix.
    Matrix row( int i ) const;

    //! Returns j-th column of this as (#cols x 1)-Matrix.
    Matrix col( int j ) const;

    //! Returns a pointer to the internal data of this.
    double * data() const;

    //! Copies the interal data (if it is also used for other matrices).
    //! If keep_allocation is true, the method will preallocate the same
    //! amount of memory it had before, regardless of the size of the matrix.
    void copy_data( bool keep_preallocation = false );

    //! Copies the Matrix M into the calling object. Does not affect matrices
    //! that previously referenced data from M. The new matrix still preallocates
    //! enough memory to store a minimum of _data_size entries.
    void copy_matrix_from( const Matrix & M );
    
    //! Returns reference to j-th column of this as (sub_rows x sub_cols)-Matrix.
    Matrix col_matrix( int j, int sub_rows, int sub_cols ) const;

    //! Scales this by alpha.
    void scale( double alpha );

    //! Computes Y = Y + alpha * X.
    //! X and Y should be of same dimensions.
    friend void matrix_add( Matrix & Y, double alpha, const Matrix & X );

    //! Computes the sclar product of two matrices.
    friend double matrix_dot( const Matrix & A, const Matrix & B );

    //! Computes C = A * B.
    friend void matrix_mul( const Matrix & A, const Matrix & B, Matrix & C );
    
    //! Computes C = A * B^T.
    friend void matrix_mul_NT( const Matrix & A, const Matrix & B, Matrix & C );
    
    //! Computes C = A^T * B.
    friend void matrix_mul_TN( const Matrix & A, const Matrix & B, Matrix & C );

    //! Matrix to ostream.
    friend ostream & operator <<( ostream & os, const Matrix & M );

    //! Matrix to ostream with prefix.
    void send_to_ostream_with_prefix( ostream & os, const string & prefix = "" ) const;
};

#endif
