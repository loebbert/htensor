#include "htensor.h"

int main()
{
    HTensor H( "../sample_tensors/tensor1.ht" );

    vector< int >   index1( 3 );
    index1[ 0 ] = 2;
    index1[ 1 ] = 1;
    index1[ 2 ] = 0;

    vector< int >   index2( 3 );
    index2[ 0 ] = 1;
    index2[ 1 ] = 2;
    index2[ 2 ] = 3;

    vector< int >   index3( 3 );
    index3[ 0 ] = 0;
    index3[ 1 ] = 0;
    index3[ 2 ] = 3;

    //cout << "entry1 = " << H.entry( index1 ) << endl;
    //cout << "entry2 = " << H.entry( index2 ) << endl;
    //cout << "entry3 = " << H.entry( index3 ) << endl;

    return 0;
}
