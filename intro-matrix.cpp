#include "matrix.h"

int main()
{
    cout << endl << "'Matrix M( 3, 4 )' constructs a new matrix of size 3x4, filled with Zeros." << endl << endl;
    Matrix M( 3, 4 );
    
    cout << "One can now set the matrix entries using the operator ()." << endl << endl;
    M( 0, 0 ) = 1;  M( 0, 1 ) = 2;  M( 0, 2 ) = 3;  M( 0, 3 ) = 4;
    M( 1, 0 ) = 5;  M( 1, 1 ) = 6;  M( 1, 2 ) = 7;  M( 1, 3 ) = 8;
    M( 2, 0 ) = 9;  M( 2, 1 ) = 10; M( 2, 2 ) = 11; M( 2, 3 ) = 12;
    
    cout << "The Matrix M can be sent to cout:" << endl;
    cout << M << endl << endl;

    cout << "'Matrix A' constructs an empty matrix (0x0)." << endl << endl;
    Matrix A;

    cout << "By the operator = one can set A as a reference to M." << endl;
    A = M;
    cout << "Now A uses the same internal data as M: if we change M ..." << endl;
    M( 1, 0 ) = -10;
    cout << "... the changes will as well appear in A (and vice versa)." << endl;
    cout << A << endl << endl;
    
    cout << "To get a real copy, use 'A.copy_data()'." << endl;
    A.copy_data();
    cout << "Now A has its own data; changes in one matrix will" << endl; 
    cout << "leave the other one unchanged." << endl;
    M( 2, 0 ) = -20;
    cout << M << endl << endl;
    cout << A << endl << endl;

    cout << "The copy constructor 'Matrix B( M )' also creates a reference!" << endl;
    Matrix B( M );
    cout << "To get a hard copy, use 'B.copy_data()'." << endl << endl;
    B.copy_data();

    cout << "The number of rows/columns can be returned:" << endl;
    cout << "B is a " << B.rows() << "x" << B.cols() << "-matrix." << endl << endl;

    cout << "'M.resize( 5, 3 )' resizes M to size 5x3, all entries are set to zero." << endl;
    M.resize( 5, 3 );
    cout << M << endl << endl;
    cout << "If there exist any references to M, these are NOT resized!" << endl;
    cout << "In this case M would get new data." << endl << endl;

    cout << "We can easily compute the matrix products C = M * A and D = A * B^T (B transposed)" << endl;
    cout << "with 'matrix_mul( M, A, C)' and 'matrix_mul_NT( A, B, D )'." << endl;
    cout << "The results C and D are resized automatically." << endl;
    Matrix C, D;
    matrix_mul( M, A, C );
    matrix_mul_NT( A, B, D );
    cout << C << endl << endl;
    cout << D << endl << endl;

    cout << "The following method will be useful as well:" << endl;
    cout << "D.row( 2 ) returns the 2nd row of D as a new 1x3-matrix:" << endl;
    cout << D.row( 2 ) << endl << endl;

    return 0;
}
