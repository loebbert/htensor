#include "matrix.h"
#include <iostream>
#include <vector>

using namespace std;

enum tensortype { ZERO };

class HTensor
{
  private:

    int                 _dim;

    bool                _is_root, _is_leaf;
    vector< int >       _cluster;
    vector< Matrix >    _data;
    HTensor             *_father, *_son1, *_son2;

  public:

    //! Constructor.
    HTensor( int dim, int n, int k, tensortype type );

    //! Constructor (HTensor from input file).
    HTensor( string filename );

    //! Copy constructor.
    HTensor( const HTensor & H );

    //! Initializes a tree node with the data according to the tensortype.
    void init( int dim, vector< int > mode_sizes, int k, tensortype type, const vector< int > & cluster, bool is_root, bool is_leaf, HTensor * father );

    //! Recursively builds a HTensor with fixed rank k, i.e. _son1 and _son2 are created until a leaf is reached.
    void build_recursively( int dim, vector< int > mode_sizes, int k, tensortype type, const vector< int > & cluster, bool is_root, bool is_leaf, HTensor * father );

    //! Default constructor.
    HTensor();

    //! Destructor.
    ~HTensor();

    int                 dim()       const   { return _dim; }
    bool                is_root()   const   { return _is_root; }
    bool                is_leaf()   const   { return _is_leaf; }
    vector< int >       cluster()   const   { return _cluster; }
    vector< Matrix >    data()      const   { return _data; }
    HTensor *           father()    const   { return _father; }
    HTensor *           son1()      const   { return _son1; }
    HTensor *           son2()      const   { return _son2; }

    //! Returns the sizes of all tensor directions as a vector.
    vector< int > mode_sizes() const;

    //! The vector is filled with the sizes of the tensor directions.
    void mode_sizes( vector< int > & mode_sizes ) const;

    //! Sets _data = data.
    void set_data( const vector< Matrix > & data );

    //! Computes the tensor entry for given tensorindex.
    double entry( const vector< int > & tensorindex ) const;

    //! Returns a HTensor, based on this, where the prescribed directions are 
    //! eliminated (i.e. reduced to size one) by summing up along them.
    HTensor sum_reduction( const vector< int > & directions ) const;

    //! Modifies one tensor entry for given tensorindex.
    void set_entry( const vector< int > & tensorindex, double new_entry );

    //! Auxiliary routine for entry(), cf. exercise sheet.
    void get_frame_row( const vector< int > & tensorindex, Matrix & M ) const;

    //! Auxiliary routine for sum_reduction(), cf. exercise sheet.
    void recursive_sum_reduction( const vector< int > & directions );

    //! Auxiliary routine for set_entry().
    //! Adds x to the tensor entry at tensorindex.
    void add_entry( const vector< int > & tensorindex, double x );

    //! Saves the tensor in a text file.
    void write_to_file( string filename, int precision = 20 ) const;

    //! Sends the content of the whole tensor to ostream.
    friend ostream & operator <<( ostream & os, const HTensor & H );

    //! Auxiliary routine.
    //! Recursively fills the vector with pointers to this and all subsequent tree nodes.
    //! The vector has to be of size 0 on input.
    void get_vector_of_all_nodes( vector< const HTensor * > & nodes ) const;
    void get_vector_of_all_nodes( vector< HTensor * > & nodes );

    //! Auxiliary routine for ostream & operator <<.
    //! Calls itself recursively for the sons.
    void send_node_content_to_ostream( ostream & os ) const;
};

//! Subdivides a cluster into son1 and son2 in a balanced way:
//! If possible, son1 and son2 are of the same size,
//! otherwise son1.size() = son2.size() - 1.
void subdivide_cluster( const vector< int > & cluster, vector< int > & son1, vector< int > & son2 );

//! Transforms vector< Matrix > to a matrix, where the vector components correspond to the columns of the matrix.
Matrix transfer_tensor_to_matrix( const vector< Matrix > & transfer_tensor );
