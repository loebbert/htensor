#include "htensor.h"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <stdexcept>

using namespace std;

//
// Constructor.
//
HTensor::HTensor( int dim, int n, int k, tensortype type )
{
    if ( dim > 1 )
    {
        vector< int > mode_sizes( dim );
        vector< int > cluster( dim );
        for ( int i = 0; i < dim; ++i )
        {
            mode_sizes[ i ] = n;
            cluster[ i ]    = i;
        }

        build_recursively( dim, mode_sizes, k, type, cluster, true, false, NULL );
    }// if ( dim > 1 )
    else
    {
        throw invalid_argument( "HTensor::HTensor : Dimension (dim) must be greater than or equal to 2." );
    }
}

//
// Constructor (HTensor from input file).
//
HTensor::HTensor( string filename )
{
    fstream file( filename.c_str(), ios::in );
    file >> _dim;
    
    vector< int > mode_sizes( _dim );
    vector< int > cluster( _dim );
    for ( int i = 0; i < _dim; ++i )
    {
        mode_sizes[ i ] = 1;
        cluster[ i ]    = i;
    }

    build_recursively( _dim, mode_sizes, 1, ZERO, cluster, true, false, NULL );

    vector< HTensor * > all_nodes;
    get_vector_of_all_nodes( all_nodes );
    for ( int i = 0; i < all_nodes.size(); ++i )
    {
        HTensor * node = all_nodes[ i ];
        int rows, cols;
        int layers = 1;
        file >> rows;
        file >> cols;
        if ( !node->_is_root && !node->_is_leaf )
        {
            file >> layers;
        }
        
        node->_data.resize( layers );
        for ( int j = 0; j < layers; ++j )
        {
            node->_data[ j ].resize( rows, cols );
            Matrix M = node->_data[ j ];
            for ( int q = 0; q < cols; ++q )
            {
                for ( int p = 0; p < rows; ++p )
                {
                    file >> M( p, q );
                }
            }
        }
    }
}

//
// Copy constructor.
//
HTensor::HTensor( const HTensor & H )
{
    _dim = H._dim;

    vector< int > mode_sizes( _dim );
    vector< int > cluster( _dim );
    for ( int i = 0; i < _dim; ++i )
    {
        mode_sizes[ i ] = 1;
        cluster[ i ]    = i;
    }

    build_recursively( _dim, mode_sizes, 1, ZERO, cluster, true, false, NULL );

    vector< HTensor * >         all_nodes;
    vector< const HTensor * >   H_all_nodes;
    get_vector_of_all_nodes( all_nodes );
    H.get_vector_of_all_nodes( H_all_nodes );

    for ( int i = 0; i < all_nodes.size(); ++i )
    {
        HTensor * node          = all_nodes[ i ];
        const HTensor * H_node  = H_all_nodes[ i ];
        int rows    = H_node->_data[ 0 ].rows();
        int cols    = H_node->_data[ 0 ].cols();
        int layers  = H_node->_data.size();

        node->_data.resize( layers );
        for ( int j = 0; j < layers; ++j )
        {
            node->_data[ j ].resize( rows, cols );
            Matrix M    = node->_data[ j ];
            Matrix H_M  = H_node->_data[ j ];
            for ( int q = 0; q < cols; ++q )
            {
                for ( int p = 0; p < rows; ++p )
                {
                    M( p, q ) = H_M( p, q );
                }
            }
        }
    }
}

//
// Initializes a tree node with the data according to the tensortype.
//
void HTensor::init( int dim, vector< int > mode_sizes, int k, tensortype type, const vector< int > & cluster, bool is_root, bool is_leaf, HTensor * father )
{
    _dim        = dim;
    _cluster    = cluster;
    _is_root    = is_root;
    _is_leaf    = is_leaf;
    _father     = father;

    switch ( type )
    {
      case ZERO:
        if ( _is_leaf )
        {
            _data.resize( 1 );
            _data[ 0 ].resize( mode_sizes[ _cluster[ 0 ] ], k );
        }
        else if ( _is_root )
        {
            // Root transfer tensor ( k x k ).
            _data.resize( 1 );
            _data[ 0 ].resize( k, k );
        }
        else
        {
            // Inner transfer tensor ( k x k x k ).
            _data.resize( k );
            for ( int i = 0; i < k; ++i )
            {
                _data[ i ].resize( k, k );
            }
        }
        break;  // case ZERO
    }// switch
}

//
// Recursively builds a HTensor with fixed rank k, i.e. _son1 and _son2 are created until a leaf is reached.
//
void HTensor::build_recursively( int dim, vector< int > mode_sizes, int k, tensortype type, const vector< int > & cluster, bool is_root, bool is_leaf, HTensor * father )
{
    // Initialize this tree node (_data gets set up according to tensortype).
    init( dim, mode_sizes, k, type, cluster, is_root, is_leaf, father );

    if ( !_is_leaf )
    {
        // Create son1 and son2.
        // Subdivision of _cluster into cluster_son1 and cluster_son2:
        vector< int >   cluster_son1;
        vector< int >   cluster_son2;
        subdivide_cluster( _cluster, cluster_son1, cluster_son2 );

        // Figure out, if son1/son2 are leaves:
        bool son1_is_leaf = false;
        bool son2_is_leaf = false;
        if ( cluster_son1.size() == 1 )
        {
            son1_is_leaf = true;
        }
        if ( cluster_son2.size() == 1 )
        {
            son2_is_leaf = true;
        }

        // Call constructor for son1 and son2:
        _son1 = new HTensor;
        _son2 = new HTensor;

        _son1->build_recursively( dim, mode_sizes, k, type, cluster_son1, false, son1_is_leaf, this );
        _son2->build_recursively( dim, mode_sizes, k, type, cluster_son2, false, son2_is_leaf, this );
    }
}

//
// Default constructor.
//
HTensor::HTensor()
{
    _dim        = 0;
    _is_root    = false;
    _is_leaf    = false;
    _father     = NULL;
    _son1       = NULL;
    _son2       = NULL;
}

//
// Destructor.
//
HTensor::~HTensor()
{
    delete _son1;
    delete _son2;
}

//
// Returns the sizes of all tensor directions as a vector.
//
vector< int > HTensor::mode_sizes() const
{
    if ( !is_root() )
    {
        return _father->mode_sizes();
    }
    vector< int > ret_vector;
    mode_sizes( ret_vector );
    return ret_vector;
}

//
// The vector is filled with the sizes of the tensor directions.
//
void HTensor::mode_sizes( vector< int > & mode_sizes ) const
{
    if ( mode_sizes.size() != _dim )
    {
        mode_sizes.resize( _dim );
    }
    if ( _is_leaf )
    {
        mode_sizes[ _cluster[ 0 ] ] = _data[ 0 ].rows();
    }
    else
    {
        _son1->mode_sizes( mode_sizes );
        _son2->mode_sizes( mode_sizes );
    }
}

void HTensor::set_data( const vector< Matrix > & data )
{
    _data = data;
    for ( int i = 0; i < data.size(); ++i )
    {
        _data[ i ].copy_data();
    }
}

//
// Computes the tensor entry for given tensorindex.
//
double HTensor::entry( const vector< int > & tensorindex ) const
{
    // Assure that this is the root.
    if ( !is_root() )
    {
        return _father->entry( tensorindex );
    }
    Matrix M;
    get_frame_row( tensorindex, M );
    return M( 0, 0 );
}

//
// Returns a HTensor, based on this, where the prescribed directions are 
// eliminated (i.e. reduced to size one) by summing up along them.
//
HTensor HTensor::sum_reduction( const vector< int > & directions ) const
{
    if ( !is_root() )
    {
        return _father->sum_reduction( directions );
    }
    HTensor reduced( *this );
    reduced.recursive_sum_reduction( directions );
    return reduced;
}

//
// Modifies one tensor entry for given tensorindex.
//
void HTensor::set_entry( const vector< int > & tensorindex, double new_entry )
{
    //
    //  TODO Exercise 3 (a)
    //
}

//
// Auxiliary routine for entry(), cf. exercise sheet.
//
void HTensor::get_frame_row( const vector< int > & tensorindex, Matrix & M ) const
{
    //
    //  TODO Exercise 1 (b)
    //
}

//
// Auxiliary routine for sum_reduction(), cf. exercise sheet.
//
void HTensor::recursive_sum_reduction( const vector< int > & directions )
{
    //
    //  TODO Exercise 2 (b)
    //
}

//
// Auxiliary routine for set_entry().
// Adds x to the tensor entry at tensorindex.
//
void HTensor::add_entry( const vector< int > & tensorindex, double x )
{
    //
    //  TODO Exercise 3 (a)
    //
}

//
// Saves the tensor in a text file.
//
void HTensor::write_to_file( string filename, int precision ) const
{
    if ( !_is_root )
    {
        _father->write_to_file( filename );
    }

    vector< const HTensor * > all_nodes;
    get_vector_of_all_nodes( all_nodes );

    fstream file( filename.c_str(), ios::out );
    
    file << _dim << endl;

    for ( int i = 0; i < all_nodes.size(); ++i )
    {
        const HTensor * node = all_nodes[ i ];
        int rows    = node->_data[ 0 ].rows();
        int cols    = node->_data[ 0 ].cols();
        file << rows << " " << cols;
        if ( !node->_is_root && !node->_is_leaf )
        {
            // Number of layers
            int layers  = node->_data.size();
            file << " " << layers;
        }
        file << endl;
        for ( int j = 0; j < node->_data.size(); ++j )
        {
            Matrix M = node->_data[ j ];
            file << setprecision( precision );
            for ( int q = 0; q < M.cols(); ++q )
            {
                for ( int p = 0; p < M.rows(); ++p )
                {
                    file << M( p, q ) << " ";
                }
                file << endl;
            }
        }
    }
}

//
// Sends the content of the whole tensor to ostream.
//
ostream & operator <<( ostream & os, const HTensor & H )
{
    if ( H._father != NULL )
    {
        operator <<( os, *H._father );
    }

    os << scientific << setprecision( 3 );
    os << "----------------------------------------------------------" << endl;
    os << "|    HIERARCHICAL TENSOR " << endl;
    os << "|        dim         = " << H._dim << endl;
    os << "|        mode sizes  : ";
    vector< int > mode_sizes = H.mode_sizes();
    for ( int i = 0; i < mode_sizes.size(); ++i )
    {
        os << mode_sizes[ i ];
        if ( i < mode_sizes.size() - 1 )
        {
            os << ", ";
        }
    }
    os << endl;
    os << "|" << endl;
    os << "|    Content:" << endl;
    os << "|" << endl;
    H.send_node_content_to_ostream( os );
    os << "----------------------------------------------------------" << endl;

    return os;
}

//
// Auxiliary routine.
// Recursively fills the vector with pointers to this and all subsequent tree nodes.
// The vector has to be of size 0 on input.
//
void HTensor::get_vector_of_all_nodes( vector< const HTensor * > & nodes ) const
{
    nodes.push_back( this );
    if ( !_is_leaf )
    {
        _son1->get_vector_of_all_nodes( nodes );
        _son2->get_vector_of_all_nodes( nodes );
    }
}

//
// Auxiliary routine.
// Recursively fills the vector with pointers to this and all subsequent tree nodes.
// The vector has to be of size 0 on input.
//
void HTensor::get_vector_of_all_nodes( vector< HTensor * > & nodes )
{
    nodes.push_back( this );
    if ( !_is_leaf )
    {
        _son1->get_vector_of_all_nodes( nodes );
        _son2->get_vector_of_all_nodes( nodes );
    }
}

//
// Used by ostream & operator <<(...). 
// Calls itself recursively for the sons.
//
void HTensor::send_node_content_to_ostream( ostream & os ) const
{
    if ( _is_root )
    {
        os << "|    Root ";
    }
    else if ( _is_leaf )
    {
        os << "|    Leaf ";
    }
    else 
    {
        os << "|    Inner ";
    }
    os << " { ";
    for ( int i = 0; i < _cluster.size(); ++i )
    {
        os << _cluster[ i ];
        if ( i < _cluster.size() - 1 )
        {
            os << ", ";
        }
    }
    os << " }" << endl;

    for ( int i = 0; i < _data.size(); ++i )
    {
        os << "|    -----------------------------------------------------" << endl;
        _data[ i ].send_to_ostream_with_prefix( os, "|    |\t" );
        os << endl;
    }
    os << "|    -----------------------------------------------------" << endl;

    os << "|" << endl;
    if ( _son1 != NULL )
    {
        _son1->send_node_content_to_ostream( os );
    }
    if ( _son2 != NULL )
    {
        _son2->send_node_content_to_ostream( os );
    }
}

//
// Subdivides a cluster into son1 and son2 in a balanced way:
// If possible, son1 and son2 are of the same size,
// otherwise son1.size() = son2.size() - 1.
//
void subdivide_cluster( const vector< int > & cluster, vector< int > & son1, vector< int > & son2 )
{
    int clustersize         = cluster.size();
    int clustersize_son1    = clustersize / 2;
    int clustersize_son2    = clustersize - clustersize_son1;
    son1.resize( clustersize_son1 );
    son2.resize( clustersize_son2 );
    for ( int i = 0; i < clustersize; ++i )
    {
        if ( i < clustersize_son1 )
        {
            son1[ i ] = cluster[ i ];
        }
        else
        {
            son2[ i - clustersize_son1 ] = cluster[ i ];
        }
    }
}

//
// Transforms vector< Matrix > to a matrix, where the vector components correspond to the columns of the matrix.
//
Matrix transfer_tensor_to_matrix( const vector< Matrix > & transfer_tensor )
{
    Matrix M( transfer_tensor[ 0 ].rows() * transfer_tensor[ 0 ].cols(), transfer_tensor.size() );

    int t_rows = transfer_tensor[ 0 ].rows();
    int t_cols = transfer_tensor[ 0 ].cols();

    for ( int j = 0; j < transfer_tensor.size(); ++j )
    {
        for ( int k = 0; k < t_cols; ++k )
        {
            for ( int i = 0; i < t_rows; ++i )
            {
                M( i + k * t_rows, j ) = transfer_tensor[ j ]( i, k );
            }
        }
    }

    return M;
}
